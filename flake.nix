{
  description = "VSCode Environment for PlatformIO";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; config.allowUnfree = true; };
        targetPkgs = p: with p; [
          gcc
          glibc
          vscode
          zlib
          (python311.withPackages (p: with p; [ setuptools ]))
          platformio
          avrdude
          esptool
          (vscode-with-extensions.override {
            vscodeExtensions = with vscode-extensions; [
              ms-vscode.cpptools
              bbenoist.nix
              ms-vscode-remote.remote-containers
            ] ++ p.vscode-utils.extensionsFromVscodeMarketplace [
              {
                name = "platformio-ide";
                publisher = "PlatformIO";
                version = "3.1.1";
                sha256 = "sha256-g9yTG3DjVUS2w9eHGAai5LoIfEGus+FPhqDnCi4e90Q=";
              }
            ];
          })
        ];
        TFT_eSPI_withUserSetup = with pkgs.stdenv; (userSetup: mkDerivation rec {
            pname = "TFT_eSPI";
            version = "2.5.33";
            src = fetchGit {
              url = "https://github.com/Bodmer/TFT_eSPI.git";
              ref = "refs/tags/v" + version;
              rev = "317ce97e598c004d158b87d01940a31acfb3eced";
            };
            inherit userSetup;
            installPhase = ''
              mkdir -p $out
              cp $src/* $out/ -r
              rm $out/User_Setup.h || true
              cp $userSetup $out/User_Setup.h -r
            '';
          });

      in
      rec {
        formatter = nixpkgs.legacyPackages.${system}.nixpkgs-fmt;
        devShell = (pkgs.mkShell {
          name = "VSCode Environment for PlatformIO";
          buildInputs = targetPkgs pkgs;
          runScript = pkgs.writeScript "init.sh" ''
            echo 'Hi there :)'
            echo Useful commands:
            echo '`pio run` - process/build project from the current directory'
            echo '`pio run --target upload` or `pio run -t upload` - upload firmware to embedded board'
            echo '`pio run --target clean` - clean project (remove compiled files)'
            echo '`pio device monitor` - open serial monitor'
          '';
        });
        packages = with pkgs.stdenv; rec {
          TFT_eSPI = TFT_eSPI_withUserSetup ./src/Setup24_ST7789_ESP32.h;

          tftSample = mkDerivation rec {
            name = "tftSample";
            src = ./.;
            TFT_eSPI = TFT_eSPI_withUserSetup ./src/Setup24_ST7789_ESP32.h;

            buildInputs = targetPkgs pkgs;
            buildPhase = ''
            '';
            installPhase = ''
              mkdir -p lib
              cp -r $TFT_eSPI lib/
              ls
              pio run
              mkdir -p $out
              cp .pio/build $out -r
            '';
          };
          upload = pkgs.writeShellScriptBin "upload" ''
            ${pkgs.platformio}/bin/pio run --target upload
          '';

          monitor = pkgs.writeShellScriptBin "monitor" ''
            ${pkgs.platformio}/bin/pio device monitor
          '';
          colourTest = mkDerivation {
            pname="colourTest";
            version="0.0";
            src=./.;
            postPatch = ''
              mkdir -p $out
              cp platformio.ini $out/
              cp -r $src/src $out/src
            '';
            nativeBuildInputs = with pkgs; [ platformio ];
          };
        };
      });
}
